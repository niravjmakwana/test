Author : Nirav Makwana
This is an example Selenium architecture created by Nirav Makwana

Technologies and tools used:

Java
Selenium Web Driver
TestNG
TestNG Reporter
Gecko Driver

package nirav_practical;

import java.io.FileInputStream;		
import java.io.FileNotFoundException;		
import java.io.IOException;		
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class google_test {

	public static void main(String[] args) throws IOException, InterruptedException  {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "C:\\\\Softwares\\Selenium\\geckodriver-v0.20.1-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver, 10);

		// Load the properties File		
	    Properties obj = new Properties();
	    FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\paths.properties");									
	    obj.load(objfile);
		driver.get(obj.getProperty("main_url"));
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Thread.sleep(5000);
		driver.findElement(By.id(obj.getProperty("searchTextBox"))).click();
		Thread.sleep(3000);
		driver.findElement(By.id(obj.getProperty("searchTextBox"))).sendKeys("nirav makwana");
		Thread.sleep(3000);
		driver.findElement(By.id(obj.getProperty("serachButton"))).click();
		Thread.sleep(10000);
		driver.quit();
		
	}
}
