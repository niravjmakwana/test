package utilities;

import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Udfunctions {
	
	public static ConfigFileReader prop = new ConfigFileReader();
	public static int timeouttime = Integer.parseInt(prop.get("implicitWaitTime"));

	public static void waitForPageToLoad(WebDriver driver, Integer maxTime) {
		WebDriverWait wait = new WebDriverWait(driver, maxTime);
	    wait.until(new ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver wdriver) {
	            return ((JavascriptExecutor) driver).executeScript(
	                "return document.readyState"
	            ).equals("complete");
	        }
	    });
	}
	 
	public static void waitFor(WebDriver driver, Integer maxTime) {
		try {
			driver.manage().timeouts().implicitlyWait(maxTime, TimeUnit.SECONDS);	
		} catch (Exception e) {
			throw(e);
		}
	}
	
	//New functions that needs to be tested
	
	//Check clicable for an element
	public static WebElement newElementToBeClickable(WebElement element, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	WebElement elementreturn = wait.until(ExpectedConditions.elementToBeClickable(element));
    	return elementreturn;
	}
	
	//Check clickable for a locator
	public static WebElement newElementToBeClickable(String xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	WebElement elementreturn = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)) );
    	return elementreturn;
	}
	
	//Check presence of Single element with locator
	public static WebElement newElementToBePresent(String xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    	return element;
	}
	
	//Check presence of Multiple element with locator
	public static List<WebElement> newElementsToBePresent(String xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	List<WebElement> elementList = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
    	return elementList;
	}
	
	//Check visibility of Single Element with Xpath
	public static WebElement newElementToBeVisible(String xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    	return element;
	}

	//Check visibility of Single Element with Element object
	public static WebElement newElementToBeVisible(WebElement element, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
    	WebElement elementreturn = wait.until(ExpectedConditions.visibilityOf(element));
    	return elementreturn;
	}
	
	//Check visibility of Multiple Elements with List<WebElement>
	public static List<WebElement> newElementsToBeVisible(List<WebElement> elementList, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
		List<WebElement> elementListReturn = wait.until(ExpectedConditions.visibilityOfAllElements(elementList));
    	return elementListReturn;
	}
	
	//Check visibility of Multiple Elements with Locator
	public static List<WebElement> newElementsToBeVisible(String xpath, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
		List<WebElement> elementList = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    	return elementList;
	}
	
	//Check page title to be same
	public static Boolean checkPageTitle(String titleValue, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
		return wait.until(ExpectedConditions.titleIs(titleValue));
	}
	
	//Check if the element is stale or still exists in the DOM
	public static Boolean checkElementState(WebElement element ,WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeouttime);
		return wait.until(ExpectedConditions.stalenessOf(element));
	}
	
}