package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.Udfunctions;

public class homePage {
	
	public static String optionsPopup = "//*[@id=\"gbwa\"]/div[2]";
	public static String optionsBtn = "//*[@id=\"gbwa\"]/div[1]/a";
	public static String googleMapsOption = "//*[@id=\"gb8\"]/span[1]";
	
	public static WebElement getOptionsButton(WebDriver driver) {
		return Udfunctions.newElementToBePresent(optionsBtn, driver);
	}
	 
	public static WebElement getOptionsPopup(WebDriver driver) {
		return Udfunctions.newElementToBePresent(optionsPopup, driver);
	}
	
	public static WebElement getGoogleMapsOption(WebDriver driver) {
		return Udfunctions.newElementToBePresent(googleMapsOption, driver);
	}

}
