package pageObject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.Udfunctions;

public class mapPage {

	public static String searchBox = "//input[@id=\"searchboxinput\"]";
	public static String searchBtn = "//button[@id=\"searchbox-searchbutton\"]";
	public static String searchResultsList = "//*[@id=\"pane\"]/div/div[1]/div/div/div[2]/div[@class=\"section-result\"]";
	public static String singleResultTitle="//h1[@class='section-hero-header-title']";
	
	
	public static WebElement getSearchBox(WebDriver driver) {
		return Udfunctions.newElementToBePresent(searchBox, driver);
	}
	
	public static WebElement getSearchBtn(WebDriver driver) {
		return Udfunctions.newElementToBePresent(searchBtn, driver);
	}
	
	public static List<WebElement> getSearchResultsList(WebDriver driver) {
		return Udfunctions.newElementsToBePresent(searchResultsList, driver);
	}
	
	public static WebElement getSingleResultTitle(WebDriver driver) {
		return Udfunctions.newElementToBePresent(singleResultTitle, driver);
	}

}
