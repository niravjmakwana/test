package testCases;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObject.homePage;
import pageObject.mapPage;
import utilities.ConfigFileReader;
import utilities.ExcelUtils;
import utilities.Udfunctions;

public class google_home {

	private static WebDriver driver;
	ConfigFileReader prop;
	Udfunctions udfunctions;

	@BeforeClass
	public void beforeClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@BeforeSuite
	public void beforeSuite() {
		prop= new ConfigFileReader();
		udfunctions= new Udfunctions();
		
		System.setProperty("webdriver.gecko.driver", prop.getDriverPath());
		
		try {
			System.out.println(prop.get("excelFilePath") + prop.get("excelFileName"));
			ExcelUtils.setExcelFile(prop.get("excelFilePath") + prop.get("excelFileName"),"Sheet1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver = new FirefoxDriver();
		Reporter.log("Browser Opened");
		
	}

	@Test
	public void goToMapsPage() {
		driver.get(prop.get("main_url"));
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		Reporter.log("Wait for page to Load");
		
		WebElement optionsButton = homePage.getOptionsButton(driver);
		udfunctions.newElementToBeVisible(optionsButton, driver);
		assertTrue(optionsButton.isDisplayed(), "Options button is visible");
		Reporter.log("Options button is visible");
		optionsButton.click();
		
		WebElement optionsPopup = homePage.getOptionsPopup(driver);
		udfunctions.newElementToBeVisible(optionsPopup, driver);
		assertTrue(optionsPopup.isDisplayed(), "Options Popup is visible");
		Reporter.log("Options Popup is visible");
		
		WebElement googleMapsOption = homePage.getGoogleMapsOption(driver);
		udfunctions.newElementToBeVisible(googleMapsOption, driver);
		assertTrue(googleMapsOption.isDisplayed(), "Google Maps Option is visible");
		Reporter.log("Google Maps Option is visible");
		googleMapsOption.click();
		
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		
		WebElement mapsSearchBox = mapPage.getSearchBox(driver);
		udfunctions.newElementToBeVisible(mapsSearchBox, driver);
		assertTrue(mapsSearchBox.isDisplayed(), "Maps Search Box is visible");
		Reporter.log("Maps Search Box is visible");
		mapsSearchBox.click();
		
		try {
			String PlaceToSearch = ExcelUtils.getCellData(1, 1);
			mapsSearchBox.sendKeys(PlaceToSearch);
			Reporter.log("Search for the Location"+PlaceToSearch);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement mapsSearchBtn = mapPage.getSearchBtn(driver);
		udfunctions.newElementToBeVisible(mapsSearchBtn, driver);
		assertTrue(mapsSearchBtn.isDisplayed(), "Maps Search Button is visible");
		Reporter.log("Maps Search Button is visible");
		mapsSearchBtn.click();
		
		udfunctions.waitFor(driver, 20);
	
		List<WebElement> searchResultsList = mapPage.getSearchResultsList(driver);	
		udfunctions.newElementsToBeVisible(searchResultsList, driver);
		for ( WebElement resultElement: searchResultsList) { 
			assertTrue(resultElement.isDisplayed(), "Search Results are visible");
			Reporter.log("Search Results are visible");
			
			try {
				ExcelUtils.setCellData("Pass", 1, 2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				try {
					ExcelUtils.setCellData("Fail", 1, 2);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
			break;
	    }

	}
	
	@Test
	public void goToMapsPage2() {
		driver.get(prop.get("main_url"));
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		Reporter.log("Wait for page to Load");
		
		WebElement optionsButton = homePage.getOptionsButton(driver);
		udfunctions.newElementToBeVisible(optionsButton, driver);
		assertTrue(optionsButton.isDisplayed(), "Options button is visible");
		Reporter.log("Options button is visible");
		optionsButton.click();
		
		WebElement optionsPopup = homePage.getOptionsPopup(driver);
		udfunctions.newElementToBeVisible(optionsPopup, driver);
		assertTrue(optionsPopup.isDisplayed(), "Options Popup is visible");
		Reporter.log("Options Popup is visible");
		
		WebElement googleMapsOption = homePage.getGoogleMapsOption(driver);
		udfunctions.newElementToBeVisible(googleMapsOption, driver);
		assertTrue(googleMapsOption.isDisplayed(), "Google Maps Option is visible");
		Reporter.log("Google Maps Option is visible");
		googleMapsOption.click();
		
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		
		WebElement mapsSearchBox = mapPage.getSearchBox(driver);
		udfunctions.newElementToBeVisible(mapsSearchBox, driver);
		assertTrue(mapsSearchBox.isDisplayed(), "Maps Search Box is visible");
		Reporter.log("Maps Search Box is visible");
		mapsSearchBox.click();
		try {
			String PlaceToSearch = ExcelUtils.getCellData(2, 1);
			mapsSearchBox.sendKeys(PlaceToSearch);
			Reporter.log("Search for the Location"+PlaceToSearch);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement mapsSearchBtn = mapPage.getSearchBtn(driver);
		udfunctions.newElementToBeVisible(mapsSearchBtn, driver);
		assertTrue(mapsSearchBtn.isDisplayed(), "Maps Search Button is visible");
		Reporter.log("Maps Search Button is visible");
		mapsSearchBtn.click();
		
		udfunctions.waitFor(driver, 20);
	
		List<WebElement> searchResultsList = mapPage.getSearchResultsList(driver);	
		udfunctions.newElementsToBeVisible(searchResultsList, driver);
		for ( WebElement resultElement: searchResultsList) { 
			assertTrue(resultElement.isDisplayed(), "Search Results are visible");
			Reporter.log("Search Results are visible");
			
			try {
				ExcelUtils.setCellData("Pass", 2, 2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				try {
					ExcelUtils.setCellData("Fail", 2, 2);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
			break;
	    }

	}
	
	@Test
	public void goToMapsPage3() {
		driver.get(prop.get("main_url"));
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		Reporter.log("Wait for page to Load");
		
		WebElement optionsButton = homePage.getOptionsButton(driver);
		udfunctions.newElementToBeVisible(optionsButton, driver);
		assertTrue(optionsButton.isDisplayed(), "Options button is visible");
		Reporter.log("Options button is visible");
		optionsButton.click();
		
		WebElement optionsPopup = homePage.getOptionsPopup(driver);
		udfunctions.newElementToBeVisible(optionsPopup, driver);
		assertTrue(optionsPopup.isDisplayed(), "Options Popup is visible");
		Reporter.log("Options Popup is visible");
		
		WebElement googleMapsOption = homePage.getGoogleMapsOption(driver);
		udfunctions.newElementToBeVisible(googleMapsOption, driver);
		assertTrue(googleMapsOption.isDisplayed(), "Google Maps Option is visible");
		Reporter.log("Google Maps Option is visible");
		googleMapsOption.click();
		
		udfunctions.waitForPageToLoad(driver, Integer.parseInt(prop.get("maxTime")));
		
		WebElement mapsSearchBox = mapPage.getSearchBox(driver);
		udfunctions.newElementToBeVisible(mapsSearchBox, driver);
		assertTrue(mapsSearchBox.isDisplayed(), "Maps Search Box is visible");
		Reporter.log("Maps Search Box is visible");
		mapsSearchBox.click();
		
		try {
			String PlaceToSearch = ExcelUtils.getCellData(3, 1);
			mapsSearchBox.sendKeys(PlaceToSearch);
			Reporter.log("Search for the Location"+PlaceToSearch);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement mapsSearchBtn = mapPage.getSearchBtn(driver);
		udfunctions.newElementToBeVisible(mapsSearchBtn, driver);
		assertTrue(mapsSearchBtn.isDisplayed(), "Maps Search Button is visible");
		Reporter.log("Maps Search Button is visible");
		mapsSearchBtn.click();
		
		udfunctions.waitFor(driver, 20);
	
		List<WebElement> searchResultsList = mapPage.getSearchResultsList(driver);	
		udfunctions.newElementsToBeVisible(searchResultsList, driver);
		for ( WebElement resultElement: searchResultsList) { 
			assertTrue(resultElement.isDisplayed(), "Search Results are visible");
			Reporter.log("Search Results are visible");
			
			try {
				ExcelUtils.setCellData("Pass", 3, 2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				try {
					ExcelUtils.setCellData("Fail", 3, 2);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
			break;
	    }

	}

	@AfterMethod
	public void afterMethod() {
	}

	@AfterClass
	public void afterClass() {
	}

	@AfterTest
	public void afterTest() {
	}

	@AfterSuite
	public void afterSuite() {
		driver.quit();
	}
}
